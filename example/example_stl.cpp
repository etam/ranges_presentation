#include <algorithm>
#include <iostream>
#include <iterator>
#include <vector>

#include "my_rand.hpp"


int main()
{
    auto v = std::vector<int>{};
    std::generate_n(std::back_inserter(v), 10, my_rand(0, 10));
    std::sort(v.begin(), v.end());
    v.erase(std::unique(v.begin(), v.end()), v.end());
    std::copy(v.begin(), v.end(), std::ostream_iterator<int>(std::cout, "\n"));
}
