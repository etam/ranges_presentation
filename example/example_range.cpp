#include <iostream>

#include <range/v3/all.hpp>

#include "my_rand.hpp"

namespace r = ranges;
namespace ra = r::action;
namespace rv = r::view;


const auto print = r::make_pipeable([](auto rng) {
    r::copy(rng, r::ostream_iterator<>(std::cout, "\n"));
});

int main()
{
    rv::generate_n(my_rand(0, 10), 10) |
        r::to_<std::vector<int>>() |
        ra::sort |
        ra::unique |
        print;
}
