#ifndef MY_RAND_HPP
#define MY_RAND_HPP

#include <random>

template <typename T>
auto my_rand(T min, T max)
{
    auto e = std::default_random_engine{std::random_device{}()};
    auto d = std::uniform_int_distribution<int>(min, max);
    return [=]() mutable {
        return d(e);
    };
}

#endif // MY_RAND_HPP
