#include <iostream>

#include <range/v3/all.hpp>
namespace r = ranges;
namespace rv = r::view;


auto filterPrime(auto rng)
{
    const auto p = r::front(rng);
    return rv::concat(
        rv::single(p),
        filterPrime(rng | rv::tail | rv::remove_if([=](auto x){ return x % p == 0; }))
    );
}

auto primes()
{
    return filterPrime(rv::ints(2));
}

int main()
{
    std::cout << (primes() | rv::take(100)) << std::endl;
}
