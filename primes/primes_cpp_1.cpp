#include <iostream>

#include <range/v3/all.hpp>
namespace r = ranges;
namespace rv = r::view;


template <typename T>
r::any_view<T> filterPrime(r::any_view<T> rng)
{
    const auto p = r::front(rng);
    return rv::concat(
        rv::single(p),
        filterPrime<T>(rng | rv::tail | rv::remove_if([=](auto x){ return x % p == 0; }))
    );
}

auto primes()
{
    return filterPrime<int>(rv::ints(2));
}

int main()
{
    std::cout << (primes() | rv::take(100)) << std::endl;
}
