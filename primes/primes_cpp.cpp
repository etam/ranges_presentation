#include <iostream>

//#include <range/v3/all.hpp>
#include <range/v3/core.hpp>
#include <range/v3/view/any_view.hpp>
#include <range/v3/view/concat.hpp>
#include <range/v3/view/iota.hpp>
#include <range/v3/view/join.hpp>
#include <range/v3/view/remove_if.hpp>
#include <range/v3/view/single.hpp>
#include <range/v3/view/tail.hpp>
#include <range/v3/view/take.hpp>
#include <range/v3/view/transform.hpp>
namespace r = ranges;
namespace rv = r::view;


template <typename Rng, typename Fun>
auto lazy(Rng&& rng, Fun&& fun)
{
    return rv::single(rng) | rv::transform(fun) | rv::join;
}

template <typename T>
r::any_view<T> filterPrime(r::any_view<T> rng)
{
    const auto p = r::front(rng);
    return rv::concat(
        rv::single(p),
        lazy(rng | rv::tail | rv::remove_if([=](auto x){ return x % p == 0; }),
             filterPrime<T>)
    );
}

auto primes()
{
    return filterPrime<int>(rv::ints(2));
}

int main()
{
    std::cout << (primes() | rv::take(100)) << std::endl;
}
