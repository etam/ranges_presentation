#include <iostream>
#include <tuple>

//#include <range/v3/all.hpp>
#include <range/v3/core.hpp>
#include <range/v3/view/iota.hpp>
#include <range/v3/view/take.hpp>
#include <range/v3/view/for_each.hpp>
namespace r = ranges;
namespace rv = ranges::view;


template <typename... Args>
std::ostream& operator<<(std::ostream& o, const std::tuple<Args...>& t)
{
    o << '(';
    r::tuple_for_each(t, [&o](const auto& v) {
        o << v << ',';
    });
    o << ')';
    return o;
}

auto pythags()
{
    return rv::for_each(rv::ints(1),    [   ](auto z) {
    return rv::for_each(rv::ints(1, z), [  z](auto x) {
    return rv::for_each(rv::ints(x, z), [x,z](auto y) {
    return r::yield_if(
        x*x + y*y == z*z,
        std::make_tuple(x, y, z)
    );});});});
}

int main()
{
    std::cout << (pythags() | rv::take(1000)) << '\n';
}
