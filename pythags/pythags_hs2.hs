-- pythags = [ (x, y, z) | z <- [1..], x <- [1..z], y <- [x..z], x^2 + y^2 == z^2 ]

import Control.Monad

pythags =
  [1..] >>= \ z ->
  [1..z] >>= \ x ->
  [x..z] >>= \ y ->
  guard (x^2 + y^2 == z^2) >>
  [(x, y, z)]

main = print $ take 1000 pythags
