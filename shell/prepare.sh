#!/bin/bash

rm -rf test
mkdir test
cd test

N=${1:-5}

for ((i=0; i<$N*3; i++)); do
    filename=$((i/3))
    minus=""
    if (( i%2 == 0 )); then
        minus="-"
    fi
    number=$((i/2))

    echo "${minus}${number}" >> $filename
done
