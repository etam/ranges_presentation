#include <iostream>
#include <memory>
#include <string>

#include <boost/filesystem.hpp>
#include <boost/filesystem/fstream.hpp>

//#include <range/v3/all.hpp>
#include <range/v3/core.hpp>
#include <range/v3/view/join.hpp>
#include <range/v3/view/remove_if.hpp>
#include <range/v3/view/single.hpp>
#include <range/v3/view/transform.hpp>

namespace fs = boost::filesystem;
namespace r = ranges;
namespace rv = r::view;


// Out: range<fs::path>
auto ls(const fs::path& path)
{
    return r::make_iterator_range(fs::directory_iterator{path},
                                  fs::directory_iterator{});
}

// Out: range<std::string>
auto read_file(const fs::path& path)
{
    return
        rv::single(std::make_shared<fs::ifstream>(path)) |
        rv::transform([](auto streamPtr) {
            return r::getlines(*streamPtr);
        }) |
        rv::join;
}

// Args: range<fs::path>
// Out: range<std::string>
const auto cat = [](auto rng) {
    return rng | rv::transform(read_file) | rv::join;
};

template <typename Fun>
auto xargs(Fun fun)
{
    return r::make_pipeable([fun](auto rng) {
        return fun(rng);
    });
}

// In:  range<std::string>
// Out: range<std::string>
auto grep(const std::string& pattern)
{
    return rv::remove_if([pattern](const std::string& line) {
        return line.find(pattern) == std::string::npos;
    });
}

// In:  range<std::string>
const auto print = r::make_pipeable([](auto rng) {
    r::copy(rng, r::ostream_iterator<>(std::cout, "\n"));
});

int main()
{
    ls("test") | xargs(cat) | grep("-") | print;
}
